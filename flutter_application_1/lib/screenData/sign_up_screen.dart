import 'package:flutter/material.dart';

class SignUpScreen extends StatefulWidget {
  static const routeName = "/sign-up-screen";
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

Widget titleSection = Container(
  padding: const EdgeInsets.all(40),
  child: Row(
    children: [
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: const Text(
                "Demo Flutter",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              "Text",
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
          ],
        ),
      ),
      Icon(
        Icons.star,
        color: Colors.blueAccent[500],
      ),
      const Text("data"),
    ],
  ),
);

Widget titleSection1 = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
        /*1*/
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /*2*/
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: const Text(
                'Oeschinen Lake Campground',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              'Kandersteg, Switzerland',
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
          ],
        ),
      ),
      /*3*/
      Icon(
        Icons.star,
        color: Colors.red[500],
      ),
      const Text('41'),
    ],
  ),
);

Widget password = Container(
  padding: const EdgeInsets.fromLTRB(20, 5, 20, 10),
  child: const TextField(
    decoration: InputDecoration(
      border: OutlineInputBorder(),
      hintText: "Password",
    ),
    style: TextStyle(
      fontWeight: FontWeight.bold,
    ),
  ),
);

class _SignUpScreenState extends State<SignUpScreen> {
  int pressCount = 0;
  void _incrementCounter() {
    setState(() {
      pressCount++;
      if (pressCount == 10) pressCount = 0;
    });
  }

  void _showDialog(String title, String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context, 'Cancel'),
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () => Navigator.pop(context, 'OK'),
                child: const Text('OK'),
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: const Text("SignUp"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: SizedBox(
                height: 350,
                width: 300,
                //child: username(),
                child: Container(
                  color: Colors.white,
                  //padding: const EdgeInsets.fromLTRB(100, 10, 100, 5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: "UserName",
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: "Password",
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: "Confirm Password",
                          ),
                        ),
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 50, vertical: 30),
                        child: SizedBox(
                          height: 40,
                          width: 200,
                          child: ElevatedButton(
                            onPressed: () => _showDialog(
                                "Create Account", "Clicked Create Account"),
                            child: const Text("Create Account"),
                          ),
                        ),
                      ),

                      // ElevatedButton(
                      //   onPressed: () => _showDialog("App", "hihi"),
                      //   child: const Text("Sign IN"),
                      // ),
                      // TextButton(
                      //   onPressed: () => const AlertDialog(
                      //     content: Text("signup"),
                      //   ),
                      //   child: const Text("SignUp"),
                      // ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Widget username() => Container(
  //       color: Colors.blue,
  //       //padding: const EdgeInsets.fromLTRB(100, 10, 100, 5),
  //       child: Column(
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         children: [
  //           TextFormField(
  //             decoration: const InputDecoration(
  //               border: UnderlineInputBorder(),
  //               labelText: "UserName",
  //             ),
  //           ),
  //           TextFormField(
  //             decoration: const InputDecoration(
  //               border: UnderlineInputBorder(),
  //               labelText: "Password",
  //             ),
  //           ),
  //           ElevatedButton(
  //             onPressed: _incrementCounter,
  //             child: Text(pressCount.toString()),
  //           ),
  //         ],
  //       ),
  //     );
}
