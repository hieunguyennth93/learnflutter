import 'package:flutter/material.dart';
import 'package:flutter_application_1/classData/product_item_provider.dart';
import 'package:flutter_application_1/classData/shopping_cart_provider.dart';
import 'package:flutter_application_1/screenData/product_detail_screen.dart';
import 'package:flutter_application_1/screenData/template.dart';
import 'package:provider/provider.dart';

class OrderProductScreen extends StatefulWidget {
  static const routeName = "/order-product-screen";
  const OrderProductScreen({Key? key}) : super(key: key);

  @override
  _OrderProductScreenState createState() => _OrderProductScreenState();
}

class _OrderProductScreenState extends State<OrderProductScreen> {
  Widget selectitem(int index) => Card(
        color: Colors.orange,
        child: Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.black,
                          width: 1.0,
                        ),
                        image: const DecorationImage(
                            image: NetworkImage('myData[index].url'),
                            fit: BoxFit.fill)),
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: Row(
                      children: const [
                        Icon(Icons.headset_rounded),
                        Text(
                          'myData.length.toString()',
                        ),
                        Icon(Icons.access_alarms),
                      ],
                    )),
              ]),
        ),
      );

  //List<ItemInformation> myData = <ItemInformation>[];

  @override
  void initState() {
    // for (int i = 0; i < 10; i++) {
    //   myData.add(ItemInformation(
    //       id: i.toString(),
    //       title: 'Data1',
    //       price: 777.0,
    //       url:
    //           'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
    //       description: 'description'));
    // }
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var deviceOrientation = MediaQuery.of(context).orientation;
    //final dataOfApplication = DataOfApplication.of(context);
    final shoppingcartList =
        Provider.of<ShoppingCartProvider>(context, listen: true)
            .getShoppingCartItems;
    final productList = Provider.of<ProductItemProvider>(context, listen: false)
        .getProductItems;

    return Scaffold(
      // appBar: AppBar(
      //   title: const Text("Mua hàng"),
      //   actions: [
      //     ShoppingcartNumber(
      //       number: dataOfApplication.getShoppingCart().length,
      //     ),
      //   ],
      // ),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            floating: true,
            pinned: false,
            snap: true,
            expandedHeight: 50.0,
            actions: [
              ShoppingcartNumber(
                number: shoppingcartList.length,
              ),
            ],
            flexibleSpace: const FlexibleSpaceBar(
              title: Text('Mua hàng'),
              background: FlutterLogo(),
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount:
                    deviceOrientation == Orientation.portrait ? 2 : 4,
                crossAxisSpacing: 4.0,
                mainAxisSpacing: 4.0),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                final productItem = productList[index];
                //final shoppingcartItem = shoppingcartList[index];
                return ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: GridTile(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(
                            context, ProductDetailScreen.routeName,
                            arguments: productItem);
                      },
                      child: Image(
                        image: NetworkImage(productItem.url),
                        fit: BoxFit.cover,
                      ),
                    ),
                    footer: GridTileBar(
                      backgroundColor: Colors.black87,
                      leading: IconButton(
                        color: Colors.red,
                        onPressed: () {
                          Provider.of<ProductItemProvider>(context,
                                  listen: false)
                              .changeFavorite(productItem, !productItem.isLove);
                        },
                        icon: productItem.isLove
                            ? const Icon(Icons.favorite)
                            : const Icon(Icons.favorite_border),
                      ),
                      title: Text(
                        productItem.title,
                        textAlign: TextAlign.center,
                      ),
                      trailing: IconButton(
                        color: Colors.blue,
                        onPressed: () {
                          Provider.of<ShoppingCartProvider>(context,
                                  listen: false)
                              .addShoppingCart(productItem);
                          Colors.black;
                        },
                        icon: ShoppingCartNumberofEachItembyID(
                            id: productItem.id.toString()),
                      ),
                    ),
                  ),
                );
              },
              childCount: productList.length,
            ),
          ),
        ],
      ),
    );
  }
}
