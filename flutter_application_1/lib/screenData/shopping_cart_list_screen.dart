import 'package:flutter/material.dart';
import 'package:flutter_application_1/classData/data_of_application.dart';
import 'package:flutter_application_1/classData/order_item_provider.dart';
import 'package:flutter_application_1/classData/product_item_provider.dart';
import 'package:flutter_application_1/classData/shopping_cart_provider.dart';
import 'package:flutter_application_1/screenData/order_payment_screen.dart';
import 'package:provider/provider.dart';

class ShoppingCartListScreen extends StatefulWidget {
  const ShoppingCartListScreen({Key? key}) : super(key: key);
  static const routeName = "/shopping-cart-list-screen";
  @override
  _ShoppingCartListScreenState createState() => _ShoppingCartListScreenState();
}

class _ShoppingCartListScreenState extends State<ShoppingCartListScreen> {
  // Widget productItem() => Row(
  //       children: [
  //         Checkbox(
  //           checkColor: Colors.white,
  //           value: false,
  //           onChanged: (bool? value) {},
  //         ),
  //         const Image(
  //           height: 110,
  //           width: 89,
  //           image: NetworkImage(
  //               "https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg"),
  //           fit: BoxFit.fitHeight,
  //         ),
  //         const SizedBox(
  //           width: 10,
  //           height: 10,
  //         ),
  //         Column(
  //           children: [
  //             const Text(
  //               "gggdsfdsafd",
  //               style: TextStyle(fontSize: 18),
  //             ),
  //             const Text(
  //               "aa",
  //               style: TextStyle(fontSize: 18, color: Colors.red),
  //             ),
  //             Row(
  //               children: [
  //                 IconButton(
  //                   onPressed: () {},
  //                   icon: const Icon(Icons.remove),
  //                   iconSize: 20,
  //                 ),
  //                 Container(
  //                   width: 25,
  //                   height: 25,
  //                   color: Colors.red,
  //                   child: const Align(
  //                     alignment: Alignment.center,
  //                     child: Text(
  //                       '2',
  //                       style: TextStyle(
  //                         fontSize: 18,
  //                       ),
  //                     ),
  //                   ),
  //                 ),
  //                 IconButton(
  //                   onPressed: () {},
  //                   icon: const Icon(Icons.add),
  //                   iconSize: 20,
  //                 ),
  //               ],
  //             )
  //           ],
  //         )
  //       ],
  //     );

  // TextEditingController _controller = TextEditingController();

  // double _count = 0;

  // void counttotal() {
  //   final dataOfApplication = DataOfApplication.of(context);
  //   for (int i = 0; i < dataOfApplication.getShoppingCart().length; i++) {
  //     _count += (dataOfApplication.getShoppingCart()[i].shoppingnumber *
  //         dataOfApplication.getShoppingCart()[i].price);
  //   }
  //   setState(() {});
  // }

  @override
  void initState() {
    //counttotal();

    // TODO: implement initState
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      debugPrint('hahahahahahah');
    });
  }

  List<int> selectedItem = [];

  bool? checkboxAll = false;
  bool? isFirstBuild = false;
  @override
  Widget build(BuildContext context) {
    final shoppingcartInfo =
        Provider.of<ShoppingCartProvider>(context, listen: false);
    final productInfo =
        Provider.of<ProductItemProvider>(context, listen: false);

    if (isFirstBuild == false) {
      selectedItem = List<int>.generate(
          shoppingcartInfo.getShoppingCartItems.length, (i) => 0);
      isFirstBuild = true;
    }
    debugPrint('build');
    return Scaffold(
      appBar: AppBar(
        title: const Text("Giỏ hàng"),
        actions: [
          IconButton(
              onPressed: () {
                if (selectedItem.contains(1) == true) {
                  shoppingcartInfo.deleteItem(selectedItem);
                  selectedItem.clear;
                  setState(() {});
                  // showDialog(
                  //     context: context,
                  //     builder: (BuildContext context) {
                  //       return AlertDialog(
                  //         title: const Text("Delete Item"),
                  //         content: const Text("Do you want to delete items"),
                  //         actions: <Widget>[
                  //           TextButton(
                  //             onPressed: () => Navigator.pop(context, 'Cancel'),
                  //             child: const Text('Cancel'),
                  //           ),
                  //           TextButton(
                  //             onPressed: () {

                  //             },
                  //             child: const Text('OK'),
                  //           ),
                  //         ],
                  //       );
                  //     });
                }
              },
              icon: const Icon(Icons.delete)),
          IconButton(onPressed: () {}, icon: const Icon(Icons.message)),
        ],
      ),
      body: Center(
        child: ListView.separated(
          itemBuilder: (context, index) {
            final shoppingcartItem =
                shoppingcartInfo.getShoppingCartItems[index];
            //final productItem = productInfo.getProductItems[index];
            return Row(
              children: [
                Checkbox(
                  checkColor: Colors.white,
                  value: selectedItem[index] == 1 ? true : false,
                  onChanged: (bool? value) {
                    selectedItem[index] = value == true ? 1 : 0;
                    setState(() {});
                  },
                ),
                Image(
                  height: 110,
                  width: 89,
                  image: NetworkImage(shoppingcartItem.url),
                  fit: BoxFit.fitHeight,
                ),
                const SizedBox(
                  width: 10,
                  height: 10,
                ),
                Column(
                  children: [
                    Text(
                      shoppingcartItem.title,
                      style: const TextStyle(fontSize: 18),
                    ),
                    Text(
                      'đ ${shoppingcartItem.price}',
                      style: const TextStyle(fontSize: 18, color: Colors.red),
                    ),
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            shoppingcartInfo
                                .removeItemCart(shoppingcartItem.id);
                            //counttotal();
                          },
                          icon: const Icon(Icons.remove),
                          iconSize: 20,
                        ),
                        Container(
                          width: 25,
                          height: 25,
                          color: Colors.red,
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              '${shoppingcartItem.quantity}',
                              style: const TextStyle(
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            shoppingcartInfo.addItemCart(shoppingcartItem.id);
                            //counttotal();
                          },
                          icon: const Icon(Icons.add),
                          iconSize: 20,
                        ),
                      ],
                    )
                  ],
                )
              ],
            );
          },
          separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
          itemCount: shoppingcartInfo.getShoppingCartItems.length,
          //itemCount: 2,
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          children: [
            Checkbox(
              checkColor: Colors.white,
              value: checkboxAll,
              onChanged: (bool? value) {
                setState(() {
                  checkboxAll = value;
                  selectedItem = List<int>.generate(
                      shoppingcartInfo.getShoppingCartItems.length,
                      (i) => value == true ? 1 : 0);
                });
              },
            ),
            const Expanded(
              flex: 1,
              child: Text("Tất cả"),
            ),
            Expanded(
              flex: 3,
              child: Text(
                  "Tổng tiền: đ ${shoppingcartInfo.totalPriceWithSelected(selectedItem)}"),
            ),
            Expanded(
              flex: 2,
              child: ElevatedButton(
                onPressed: () {
                  Provider.of<OrderItemProvider>(context, listen: false)
                      .addOrder(shoppingcartInfo.getShoppingCartItems);
                  shoppingcartInfo.getShoppingCartItems.clear();
                  selectedItem.clear();

                  Navigator.of(context)
                      .popAndPushNamed(OrderPaymentScreen.routeName);
                },
                child: const Text("Thanh toán"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
