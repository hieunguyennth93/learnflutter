import 'package:flutter/material.dart';
import 'package:flutter_application_1/classData/order_item_provider.dart';
import 'package:provider/provider.dart';

class OrderPaymentScreen extends StatefulWidget {
  static const routeName = '/order-payment-screen';
  const OrderPaymentScreen({Key? key}) : super(key: key);

  @override
  _OrderPaymentScreenState createState() => _OrderPaymentScreenState();
}

class _OrderPaymentScreenState extends State<OrderPaymentScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Payment"),
      ),
      body: Builder(builder: (context) {
        final orderItem = Provider.of<OrderItemProvider>(context).getOrderItems;
        return ListView.separated(
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(orderItem[index].title),
                subtitle: Text(
                  orderItem[index].price.toString(),
                ),
              );
            },
            separatorBuilder: (context, index) => const Divider(),
            itemCount: orderItem.length);
      }),
    );
  }
}
