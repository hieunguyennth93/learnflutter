import 'package:flutter/material.dart';
import '../classData/product_item_information.dart';

class MyShopProductNewAndEditScreen extends StatefulWidget {
  final ProductItemInformation? itemInformation;
  const MyShopProductNewAndEditScreen({Key? key, this.itemInformation})
      : super(key: key);

  static const routeName = '/my-shop-product-new-and-edit-screen';
  @override
  _MyShopProductNewAndEditScreenState createState() =>
      _MyShopProductNewAndEditScreenState();
}

class _MyShopProductNewAndEditScreenState
    extends State<MyShopProductNewAndEditScreen> {
  final TextEditingController titleController = TextEditingController();

  final TextEditingController urlController = TextEditingController();
  final Map<String, dynamic> myData = {
    'id': '',
    'title': '',
    'price': '',
    'url': '',
    'description': '',
    'islove': false,
  };

  @override
  void didChangeDependencies() {
    final result =
        ModalRoute.of(context)!.settings.arguments as ProductItemInformation?;
    if (result != null) {
      myData['id'] = result.id.toString();
      myData['title'] = result.title;
      myData['price'] = result.price.toString();
      myData['url'] = result.url;
      myData['description'] = result.description;
    }
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  // @override
  // void initState() {
  //   if (widget.itemInformation != null) {
  //     myData['id'] = widget.itemInformation!.id.toString();
  //     myData['title'] = widget.itemInformation!.title;
  //     myData['price'] = widget.itemInformation!.price.toString();
  //     myData['url'] = widget.itemInformation!.url;
  //     myData['description'] = widget.itemInformation!.description;

  //     //titleController.text = myData['title'];
  //   }
  //   super.initState();
  // }

  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Create new product"),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              if (formKey.currentState!.validate()) {
                formKey.currentState?.save();
                //addDatatoMap();
                Navigator.pop(
                    context,
                    ProductItemInformation(
                        id: myData['id'],
                        title: myData['title'],
                        price: double.parse(myData['price']),
                        url: myData['url'],
                        description: myData['description'],
                        isLove: false));
              }
            },
            icon: const Icon(Icons.save),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: TextFormField(
                  onSaved: (String? value) {
                    myData['id'] = value ?? '';
                  },
                  initialValue: myData['id'],
                  //controller: titleController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Nhập sai định dạng';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: "Mã sản phẩm",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: TextFormField(
                  onSaved: (String? value) {
                    myData['title'] = value ?? '';
                  },
                  initialValue: myData['title'],
                  //controller: titleController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Nhập sai định dạng';
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: "Tên sản phẩm",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: TextFormField(
                  onSaved: (String? value) {
                    myData['price'] = value ?? 0;
                  },
                  initialValue: myData['price'],
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: "Giá",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: TextFormField(
                  onSaved: (String? value) {
                    myData['description'] = value ?? '';
                  },
                  initialValue: myData['description'],
                  decoration: const InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: "Mô tả",
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 40, left: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(
                      height: 150,
                      width: 150,
                      child: Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black,
                              width: 1.0,
                            ),
                            image: DecorationImage(
                                image: NetworkImage(myData['url']),
                                fit: BoxFit.fill)),
                      ),
                    ),
                    const SizedBox(
                      height: 150,
                      width: 10,
                    ),
                    Expanded(
                      flex: 1,
                      child: TextFormField(
                        initialValue: myData['url'],
                        //controller: urlController,
                        validator: (context) {
                          if (context == null ||
                              context.isEmpty ||
                              !context.contains("http")) {
                            return 'Nhập sai định dạng';
                          }
                          return null;
                        },
                        onChanged: (String? value) {
                          setState(() {
                            myData['url'] = value ?? '';
                          });
                        },
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: "Đường dẫn URL",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
