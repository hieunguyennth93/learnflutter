import 'package:flutter/material.dart';
import 'package:flutter_application_1/classData/data_of_application.dart';
import 'package:flutter_application_1/classData/product_item_information.dart';
import 'package:flutter_application_1/classData/product_item_provider.dart';
import 'package:flutter_application_1/screenData/my_shop_product_new_and_edit_screen.dart';
import 'package:provider/provider.dart';

class MyShopProductListScreen extends StatefulWidget {
  static const routeName = "/my-shop-product-list-screen";
  const MyShopProductListScreen({Key? key}) : super(key: key);

  @override
  _MyShopProductListScreenState createState() =>
      _MyShopProductListScreenState();
}

class _MyShopProductListScreenState extends State<MyShopProductListScreen> {
  String image =
      'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg';

  // Widget item(String title, String url, int index, DataOfApplication data) =>
  //     Row(
  //       children: <Widget>[
  //         Expanded(
  //           flex: 2,
  //           child: CircleAvatar(
  //             radius: 40,
  //             backgroundColor: Colors.black,
  //             child: CircleAvatar(
  //               radius: 38,
  //               backgroundImage: NetworkImage(url),
  //             ),
  //           ),
  //         ),
  //         Expanded(
  //           flex: 4,
  //           child: Container(
  //             margin: const EdgeInsets.only(left: 10),
  //             height: 100,
  //             child: Align(
  //               alignment: Alignment.centerLeft,
  //               child: Text(
  //                 title,
  //                 style: const TextStyle(fontSize: 20),
  //               ),
  //             ),
  //           ),
  //         ),
  //         Expanded(
  //             flex: 1,
  //             child: IconButton(
  //               icon: const Icon(
  //                 Icons.edit,
  //                 color: Colors.purple,
  //               ),
  //               onPressed: () async {
  //                 // final result = await Navigator.push(
  //                 //   context,
  //                 //   MaterialPageRoute(
  //                 //     builder: (context) => ProductNew(
  //                 //       itemInformation: myData[index],
  //                 //     ),
  //                 //   ),
  //                 // );
  //                 // final result = await Navigator.pushNamed(
  //                 //     context, ProductNewAndEdit.routeName,
  //                 //     arguments: dataOfApplication.[index]);
  //                 // if (result != null) {
  //                 //   myData.removeAt(index);
  //                 //   myData.insert(index, result as ItemInformation);
  //                 //   setState(() {});
  //                 // }
  //               },
  //             )),
  //         Expanded(
  //           flex: 1,
  //           child: IconButton(
  //             icon: const Icon(
  //               Icons.delete,
  //               color: Colors.red,
  //             ),
  //             onPressed: () {
  //               setState(() {
  //                 //data. .removeAt(index);
  //               });
  //             },
  //           ),
  //         ),
  //       ],
  //     );

  //List<ItemInformation> myData = <ItemInformation>[];
  //Map myData = <String, String>{};

  @override
  void initState() {
    // for (int i = 0; i < 10; i++) {
    //   myData.add(ItemInformation(
    //       id: i.toString(),
    //       title: 'Data1',
    //       price: 777.0,
    //       url:
    //           'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
    //       description: 'description',
    //       shoppingnumber: 0));
    // }
    // TODO: implement initState
    super.initState();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    //final dataOfApplication = DataOfApplication.of(context);
    final productInfo =
        Provider.of<ProductItemProvider>(context, listen: false);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text("Danh sách sản phẩm"),
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {
            _scaffoldKey.currentState!.openDrawer();
          },
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.filter),
          ),
          IconButton(
            onPressed: () async {
              final result = await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          const MyShopProductNewAndEditScreen()));
              //myData.add();
              setState(() {
                if (result != null) {
                  productInfo.addProduct(result as ProductItemInformation);
                }
              });

              //debugPrint('${myData.length}');
            },
            icon: const Icon(Icons.add),
          ),
        ],
      ),
      body: Center(
        child: ListView.separated(
            padding: const EdgeInsets.all(8),
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
            itemCount: productInfo.getProductItems.length,
            itemBuilder: (BuildContext context, int index) {
              // return item('${dataOfApplication.getItemInfo()[index].title}',
              //     '${dataOfApplication.getItemInfo()[index].url}', index, dataOfApplication);
              return Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: CircleAvatar(
                      radius: 40,
                      backgroundColor: Colors.black,
                      child: CircleAvatar(
                        radius: 38,
                        backgroundImage: NetworkImage(
                            productInfo.getProductItems[index].url),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Container(
                      margin: const EdgeInsets.only(left: 10),
                      height: 100,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          productInfo.getProductItems[index].title,
                          style: const TextStyle(fontSize: 20),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                      flex: 1,
                      child: IconButton(
                        icon: const Icon(
                          Icons.edit,
                          color: Colors.purple,
                        ),
                        onPressed: () async {
                          final result = await Navigator.pushNamed(
                              context, MyShopProductNewAndEditScreen.routeName,
                              arguments: productInfo.getProductItems[index]);
                          if (result != null) {
                            productInfo.updateProduct(
                                result as ProductItemInformation);
                            setState(() {});
                          }
                        },
                      )),
                  Expanded(
                    flex: 1,
                    child: IconButton(
                      icon: const Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        setState(() {
                          productInfo.removeProduct(
                              productInfo.getProductItems[index]);
                          //data. .removeAt(index);
                        });
                      },
                    ),
                  ),
                ],
              );
            }),
      ),
    );
  }
}
