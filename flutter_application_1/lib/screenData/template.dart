import 'package:flutter/material.dart';
import 'package:flutter_application_1/classData/data_of_application.dart';
import 'package:flutter_application_1/classData/shopping_cart_provider.dart';
import 'package:flutter_application_1/screenData/shopping_cart_list_screen.dart';
import 'package:provider/provider.dart';

class DrawerMenu extends StatelessWidget {
  const DrawerMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Center(
              child: Text('User Logo here'),
            ),
          ),
          ListTile(
            title: const Text('Product List'),
            onTap: () {
              // Update the state of the app
              // Navigator.pushReplacementNamed(
              //     context, ProductListScreen.routeName);
              // Then close the drawer
              // Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Shop Product List'),
            onTap: () {
              // Update the state of the app
              // Navigator.pushReplacementNamed(
              //     context, ShopProductListScreen.routeName);
              // Then close the drawer
              // Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}

class ShoppingcartNumber extends StatelessWidget {
  final int? number;
  const ShoppingcartNumber({Key? key, this.number}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        IconButton(
            onPressed: () {
              Navigator.pushNamed(context, ShoppingCartListScreen.routeName);
            },
            icon: const Icon(Icons.shopping_cart)),
        if (number != null && number != 0)
          Positioned(
            right: 5,
            top: 10,
            child: Container(
              padding: const EdgeInsets.all(1),
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(8.5),
              ),
              constraints: const BoxConstraints(
                minWidth: 15,
                minHeight: 15,
              ),
              child: Text(
                '$number',
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 10,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          )
      ],
    );
  }
}

class ShoppingCartNumberofEachItembyID extends StatelessWidget {
  String? id;
  ShoppingCartNumberofEachItembyID({Key? key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //final dataOfApplication = DataOfApplication.of(context);
    final shoppingcartInfo =
        Provider.of<ShoppingCartProvider>(context, listen: false);
    final cnt = shoppingcartInfo.getShoppingCartItems
        .indexWhere((element) => id == element.id);
    if (cnt != -1) {
      id = shoppingcartInfo.getShoppingCartItems[cnt].quantity.toString();
    } else {
      id = null;
    }
    return Stack(
      children: [
        const Icon(
          Icons.shopping_cart,
        ),
        if (id != null && id != "0")
          Positioned(
            left: 10,
            //top: 0,
            child: Container(
              padding: const EdgeInsets.all(1),
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(8.5),
              ),
              constraints: const BoxConstraints(
                minWidth: 15,
                minHeight: 15,
              ),
              child: Text(
                '$id',
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 10,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          )
      ],
    );
  }
}
