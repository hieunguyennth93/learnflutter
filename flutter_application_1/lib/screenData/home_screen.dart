import 'package:flutter/material.dart';
import 'package:flutter_application_1/screenData/my_shop_product_list_screen.dart';
import 'package:flutter_application_1/screenData/shopping_cart_list_screen.dart';
import 'package:flutter_application_1/screenData/template.dart';

import 'order_product_screen.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = "/home-screen";
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int selectedIndex = 0;

  Future<void> _onItemTapped(int index) async {
    selectedIndex = index;
    if (index == 1) {
      await Navigator.pushNamed(context, MyShopProductListScreen.routeName);
    } else if (index == 2) {
      await Navigator.pushNamed(context, OrderProductScreen.routeName);
    } else if (index == 3) {
      await Navigator.pushNamed(context, ShoppingCartListScreen.routeName);
    }
    selectedIndex = 0;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text("Home Page"),
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {
            _scaffoldKey.currentState!.openDrawer();
          },
        ),
      ),
      drawer: const DrawerMenu(),
      drawerEnableOpenDragGesture: false,
      body: const SingleChildScrollView(
        child: Center(
          child: Text("This page to display advertise and news"),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Trang chủ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Gian hàng của tôi',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag),
            label: 'Mua hàng',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: 'Giỏ hàng',
          ),
        ],
        currentIndex: selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: (index) => _onItemTapped(index),
      ),
    );
  }
}
