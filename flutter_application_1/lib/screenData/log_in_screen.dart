import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/screenData/home_screen.dart';
import 'package:flutter_application_1/screenData/my_shop_product_list_screen.dart';
import 'package:flutter_application_1/screenData/my_shop_product_new_and_edit_screen.dart';
import 'package:flutter_application_1/screenData/sign_up_screen.dart';

enum AuthMode { login, signup, forgotpassword }

class LogInScreen extends StatefulWidget {
  static const routeName = "/log-in-screen";
  const LogInScreen({Key? key}) : super(key: key);

  @override
  _LogInScreenState createState() => _LogInScreenState();
}

class _LogInScreenState extends State<LogInScreen> {
  AuthMode _authMode = AuthMode.login;

  void changeMode(int mode) {
    debugPrint('$mode');
    setState(() {
      switch (mode) {
        case 0:
          {
            _authMode = AuthMode.login;
            debugPrint("Login");
          }
          break;
        case 1:
          {
            _authMode = AuthMode.signup;
            debugPrint("Signup");
          }
          break;
        case 2:
          {
            _authMode = AuthMode.forgotpassword;
            debugPrint("Forgotpassword");
          }
          break;
        default:
          {}
          break;
      }
    });
  }

  void _showDialog(String title, String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context, 'Cancel'),
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () => Navigator.pop(context, 'OK'),
                child: const Text('OK'),
              ),
            ],
          );
        });
  }

  Widget layoutlogin() => Column(
        children: [
          TextFormField(
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "UserName",
            ),
          ),
          TextFormField(
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Password",
            ),
            obscureText: true,
          ),
          TextButton(
            onPressed: () {
              changeMode(AuthMode.forgotpassword.index);
            },
            child: const Text('Forgot Password'),
          ),
          Container(
            width: 120,
            child: ElevatedButton(
              onPressed: () {
                changeMode(AuthMode.login.index);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const HomeScreen()));
              },
              child: const Text("Login"),
            ),
          ),
          TextButton(
            onPressed: () {
              changeMode(AuthMode.signup.index);
            },
            child: const Text("Create Account"),
          ),
        ],
      );

  Widget layoutforgotpassword() => Column(
        children: [
          const SizedBox(height: 10),
          TextFormField(
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Input your email address",
            ),
          ),
          const SizedBox(height: 10),
          Container(
            width: 180,
            child: ElevatedButton(
              onPressed: () {
                changeMode(AuthMode.login.index);
                _showDialog("Forgot Password",
                    "If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.");
              },
              child: const Text("Reset Password"),
            ),
          ),
          const SizedBox(height: 10),
        ],
      );

  Widget layoutsignup() => Column(
        children: [
          const SizedBox(height: 10),
          TextFormField(
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Input your email address or username",
            ),
          ),
          const SizedBox(height: 10),
          TextFormField(
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Password",
            ),
            obscureText: true,
          ),
          const SizedBox(height: 10),
          TextFormField(
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Confirm Password",
            ),
            obscureText: true,
          ),
          const SizedBox(height: 10),
          Container(
            width: 180,
            child: ElevatedButton(
              onPressed: () {
                changeMode(AuthMode.login.index);
              },
              child: const Text("Create Account"),
            ),
          ),
          const SizedBox(height: 10),
        ],
      );

  @override
  Widget build(BuildContext context) {
    var deviceOrientation = MediaQuery.of(context).orientation;
    if (deviceOrientation == Orientation.portrait) {
      return Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(
          title: const Text("SignIn"),
        ),
        body: Stack(
          children: [
            Container(),
            Center(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 50, vertical: 10),
                      transform: Matrix4.rotationZ(-0.1),
                      decoration: BoxDecoration(
                        color: Colors.deepOrange.shade900,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: const Text(
                        'My Logo',
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: const EdgeInsets.all(30),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Form(
                          child: Column(
                            children: [
                              if (_authMode == AuthMode.login) layoutlogin(),
                              if (_authMode == AuthMode.forgotpassword)
                                layoutforgotpassword(),
                              if (_authMode == AuthMode.signup) layoutsignup(),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(
          title: const Text("SignIn_Landscape"),
        ),
        body: Stack(
          children: [
            Container(),
            Center(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      margin: const EdgeInsets.symmetric(
                          horizontal: 160, vertical: 30),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 50),
                        child: Form(
                          child: Column(
                            children: [
                              TextFormField(
                                decoration: const InputDecoration(
                                  border: UnderlineInputBorder(),
                                  labelText: "UserName",
                                ),
                              ),
                              TextFormField(
                                decoration: const InputDecoration(
                                  border: UnderlineInputBorder(),
                                  labelText: "Password",
                                ),
                                obscureText: true,
                              ),
                              if (_authMode == AuthMode.signup)
                                TextFormField(
                                  decoration: const InputDecoration(
                                    labelText: 'Confirm Password',
                                  ),
                                  obscureText: true,
                                ),
                              TextButton(
                                onPressed: () {
                                  changeMode(AuthMode.forgotpassword.index);
                                },
                                child: const Text('Forgot Password'),
                              ),
                              if (_authMode == AuthMode.login)
                                ElevatedButton(
                                  onPressed: () {
                                    changeMode(AuthMode.login.index);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                const HomeScreen()));
                                  },
                                  child: const Text("Login"),
                                ),
                              if (_authMode == AuthMode.login)
                                TextButton(
                                  onPressed: () {
                                    changeMode(AuthMode.signup.index);
                                  },
                                  child: const Text("Create Account"),
                                ),
                              if (_authMode == AuthMode.signup)
                                ElevatedButton(
                                  onPressed: () {
                                    changeMode(AuthMode.login.index);
                                  },
                                  child: const Text("Create Account"),
                                ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    }
  }
}
