import 'package:flutter/material.dart';
import 'package:flutter_application_1/classData/product_item_information.dart';

class ProductItemProvider extends ChangeNotifier {
  //final List<ProductItemInformation> _productitemInfo = [];

  final List<ProductItemInformation> _productitemInfo =
      <ProductItemInformation>[
    ProductItemInformation(
        id: '1',
        title: 'Product 1',
        price: 90.0,
        url:
            'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
        description: 'Sản phẩm bán chạy nhất mọi thời đại',
        isLove: false),
    ProductItemInformation(
        id: '2',
        title: 'Product 2',
        price: 500,
        url:
            'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
        description: 'Sản phẩm bán chạy nhất 2021',
        isLove: false),
    ProductItemInformation(
        id: '3',
        title: 'Product 3',
        price: 300,
        url:
            'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
        description: 'Sản phẩm bán chạy nhất 2022',
        isLove: false),
    ProductItemInformation(
        id: '4',
        title: 'Product 4',
        price: 250,
        url:
            'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
        description: 'Sản phẩm bán chạy nhất 2019',
        isLove: false),
    ProductItemInformation(
        id: '5',
        title: 'Product 5',
        price: 300,
        url:
            'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
        description: 'Sản phẩm bán chạy nhất 2022',
        isLove: false),
    ProductItemInformation(
        id: '6',
        title: 'Product 6',
        price: 300,
        url:
            'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
        description: 'Sản phẩm bán chạy nhất 2022',
        isLove: false),
    ProductItemInformation(
        id: '7',
        title: 'Product 7',
        price: 300,
        url:
            'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
        description: 'Sản phẩm bán chạy nhất 2022',
        isLove: false),
    ProductItemInformation(
        id: '8',
        title: 'Product 8',
        price: 300,
        url:
            'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
        description: 'Sản phẩm bán chạy nhất 2022',
        isLove: false),
    ProductItemInformation(
        id: '9',
        title: 'Product 9',
        price: 300,
        url:
            'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
        description: 'Sản phẩm bán chạy nhất 2022',
        isLove: false),
  ];

  List<ProductItemInformation> get getProductItems {
    return [..._productitemInfo];
  }

  void addProduct(ProductItemInformation itemInformation) {
    _productitemInfo.add(itemInformation);
  }

  void removeProduct(ProductItemInformation itemInformation) {
    _productitemInfo.removeWhere((element) => itemInformation.id == element.id);
  }

  void updateProduct(ProductItemInformation itemInformation) {
    final index = _productitemInfo
        .indexWhere((element) => itemInformation.id == element.id);
    _productitemInfo[index] = itemInformation;
  }

  void changeFavorite(ProductItemInformation itemInformation, bool isLoved) {
    int index = _productitemInfo
        .indexWhere((element) => itemInformation.id == element.id);
    _productitemInfo[index].isLove = isLoved;
  }
}
