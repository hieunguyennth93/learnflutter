class ProductItemInformation {
  final String id;
  final String title;
  final double price;
  final String url;
  final String description;
  bool isLove;

  ProductItemInformation({
    required this.id,
    required this.title,
    required this.price,
    required this.url,
    required this.description,
    required this.isLove,
  });
}

// class ShoppingCart {
//   String id;
//   String title;
//   double price;
//   String url;
//   int quantity;
//   ShoppingCart(
//       {required this.id,
//       required this.title,
//       required this.price,
//       required this.url,
//       required this.quantity});
// }
