// import 'package:flutter/material.dart';
// import 'package:flutter_application_1/classData/product_item_information.dart';
// import 'package:flutter_application_1/classData/shopping_cart_information.dart';

// class DataOfApplication extends StatefulWidget {
//   final Widget child;
//   const DataOfApplication({Key? key, required this.child}) : super(key: key);

//   static _DataOfApplicationState of(BuildContext context) {
//     return (context
//             .dependOnInheritedWidgetOfExactType<_ProductInheritedWidget>())!
//         .data;
//   }

//   @override
//   _DataOfApplicationState createState() => _DataOfApplicationState();
// }

// class _DataOfApplicationState extends State<DataOfApplication> {
//   final List<ShoppingCartInformation> shoppingcartInfo =
//       <ShoppingCartInformation>[];

//   final List<ProductItemInformation> itemInfo = <ProductItemInformation>[
//     ProductItemInformation(
//         id: '1',
//         title: 'Product 1',
//         price: 90.0,
//         url:
//             'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
//         description: 'Sản phẩm bán chạy nhất mọi thời đại',
//         isLove: false),
//     ProductItemInformation(
//         id: '2',
//         title: 'Product 2',
//         price: 500,
//         url:
//             'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
//         description: 'Sản phẩm bán chạy nhất 2021',
//         isLove: false),
//     ProductItemInformation(
//         id: '3',
//         title: 'Product 3',
//         price: 300,
//         url:
//             'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
//         description: 'Sản phẩm bán chạy nhất 2022',
//         isLove: false),
//     ProductItemInformation(
//         id: '4',
//         title: 'Product 4',
//         price: 250,
//         url:
//             'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
//         description: 'Sản phẩm bán chạy nhất 2019',
//         isLove: false),
//     ProductItemInformation(
//         id: '5',
//         title: 'Product 5',
//         price: 300,
//         url:
//             'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
//         description: 'Sản phẩm bán chạy nhất 2022',
//         isLove: false),
//     ProductItemInformation(
//         id: '6',
//         title: 'Product 6',
//         price: 300,
//         url:
//             'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
//         description: 'Sản phẩm bán chạy nhất 2022',
//         isLove: false),
//     ProductItemInformation(
//         id: '7',
//         title: 'Product 7',
//         price: 300,
//         url:
//             'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
//         description: 'Sản phẩm bán chạy nhất 2022',
//         isLove: false),
//     ProductItemInformation(
//         id: '8',
//         title: 'Product 8',
//         price: 300,
//         url:
//             'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
//         description: 'Sản phẩm bán chạy nhất 2022',
//         isLove: false),
//     ProductItemInformation(
//         id: '9',
//         title: 'Product 9',
//         price: 300,
//         url:
//             'https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg',
//         description: 'Sản phẩm bán chạy nhất 2022',
//         isLove: false),
//   ];
//   // init default product items
//   List<ProductItemInformation> getItemInfo() {
//     return itemInfo;
//   }

//   // List<ShoppingCart> getShoppingCart() {
//   //   return shoppingcartInfo;
//   // }

//   // void addShoppingCart(ItemInformation itemInformation) {
//   //   setState(() {
//   //     final index = shoppingcartInfo
//   //         .indexWhere((element) => itemInformation.id == element.id);
//   //     if (index == -1) {
//   //       shoppingcartInfo.add(ShoppingCart(
//   //           id: itemInformation.id,
//   //           title: itemInformation.title,
//   //           price: itemInformation.price,
//   //           url: itemInformation.url,
//   //           quantity: 1));
//   //     } else {
//   //       shoppingcartInfo[index].quantity += 1;
//   //     }
//   //   });
//   // }

//   // void addItemCart(String idOfProduct) {
//   //   setState(() {
//   //     final index =
//   //         shoppingcartInfo.indexWhere((element) => idOfProduct == element.id);
//   //     if (index == -1) {
//   //       //return null;
//   //     } else {
//   //       shoppingcartInfo[index].quantity += 1;
//   //     }
//   //   });
//   // }

//   // void removeItemCart(String idOfProduct) {
//   //   setState(() {
//   //     final index =
//   //         shoppingcartInfo.indexWhere((element) => idOfProduct == element.id);
//   //     if (index == -1) {
//   //       //return null;
//   //     } else {
//   //       if (shoppingcartInfo[index].quantity == 1) {
//   //         shoppingcartInfo.removeAt(index);
//   //       } else {
//   //         shoppingcartInfo[index].quantity -= 1;
//   //       }
//   //     }
//   //   });
//   // }

//   // void deleteItem(List<int> selectedItem) {
//   //   for (int i = 0; i < selectedItem.length; i++) {
//   //     int ind = selectedItem.indexWhere((element1) => element1 == 1);
//   //     if (ind != -1) {
//   //       selectedItem.removeAt(ind);
//   //       shoppingcartInfo.removeAt(ind);
//   //     }
//   //   }
//   //   //shoppingcartInfo.removeWhere((element) => selectedItem.indexWhere((element1) => element1 == 1) );

//   //   // List<ShoppingCart> temp = shoppingcartInfo;
//   //   // for (int i = 0; i < selectedItem.length; i++) {
//   //   //   if (selectedItem[i] == 1) {
//   //   //     shoppingcartInfo.removeWhere((element) => element.id == temp[i].id);
//   //   //   }
//   //   // }
//   // }

//   // double totalPriceWithSelected(List<int> selectedItem) {
//   //   double total = 0;

//   //   for (int i = 0; i < shoppingcartInfo.length; i++) {
//   //     if (selectedItem[i] == 1) {
//   //       total += shoppingcartInfo[i].price * shoppingcartInfo[i].quantity;
//   //     }
//   //   }

//   //   return total;
//   // }

//   //get items => itemInfo;
//   void addProduct(ProductItemInformation itemInformation) {
//     setState(() {
//       itemInfo.add(itemInformation);
//     });
//   }

//   void removeProduct(ProductItemInformation itemInformation) {
//     setState(() {
//       itemInfo.removeWhere((element) => itemInformation.id == element.id);
//     });
//   }

//   void updateProduct(ProductItemInformation itemInformation) {
//     setState(() {
//       final index =
//           itemInfo.indexWhere((element) => itemInformation.id == element.id);
//       itemInfo[index] = itemInformation;
//     });
//   }

//   void changeFavorite(ProductItemInformation itemInformation, bool isLoved) {
//     setState(() {
//       int index =
//           itemInfo.indexWhere((element) => itemInformation.id == element.id);
//       itemInfo[index].isLove = isLoved;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return _ProductInheritedWidget(
//       data: this,
//       child: widget.child,
//     );
//   }
// }

// class _ProductInheritedWidget extends InheritedWidget {
//   final _DataOfApplicationState data;

//   const _ProductInheritedWidget(
//       {Key? key, required this.data, required Widget child})
//       : super(key: key, child: child);

//   @override
//   bool updateShouldNotify(covariant InheritedWidget oldWidget) {
//     return true;
//   }
// }
