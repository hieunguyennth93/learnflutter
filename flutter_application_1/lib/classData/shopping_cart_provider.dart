import 'package:flutter/material.dart';
import 'package:flutter_application_1/classData/product_item_information.dart';
import 'package:flutter_application_1/classData/shopping_cart_information.dart';

class ShoppingCartProvider extends ChangeNotifier {
  final List<ShoppingCartInformation> _shoppingcartInfo = [];

  List<ShoppingCartInformation> get getShoppingCartItems {
    return [..._shoppingcartInfo];
  }

  void addShoppingCart(ProductItemInformation itemInformation) {
    final index = _shoppingcartInfo
        .indexWhere((element) => itemInformation.id == element.id);
    if (index == -1) {
      _shoppingcartInfo.add(ShoppingCartInformation(
          id: itemInformation.id,
          title: itemInformation.title,
          price: itemInformation.price,
          url: itemInformation.url,
          quantity: 1));
    } else {
      _shoppingcartInfo[index].quantity += 1;
    }
    notifyListeners();
  }

  void addItemCart(String idOfProduct) {
    final index =
        _shoppingcartInfo.indexWhere((element) => idOfProduct == element.id);
    if (index == -1) {
      //return null;
    } else {
      _shoppingcartInfo[index].quantity += 1;
    }
    notifyListeners();
  }

  void removeItemCart(String idOfProduct) {
    final index =
        _shoppingcartInfo.indexWhere((element) => idOfProduct == element.id);
    if (index == -1) {
      //return null;
    } else {
      if (_shoppingcartInfo[index].quantity == 1) {
        _shoppingcartInfo.removeAt(index);
      } else {
        _shoppingcartInfo[index].quantity -= 1;
      }
    }
    notifyListeners();
  }

  void deleteItem(List<int> selectedItem) {
    for (int i = 0; i < selectedItem.length; i++) {
      int ind = selectedItem.indexWhere((element1) => element1 == 1);
      if (ind != -1) {
        selectedItem.removeAt(ind);
        _shoppingcartInfo.removeAt(ind);
      }
    }
    notifyListeners();
  }

  double totalPriceWithSelected(List<int> selectedItem) {
    double total = 0;

    for (int i = 0; i < _shoppingcartInfo.length; i++) {
      if (selectedItem[i] == 1) {
        total += _shoppingcartInfo[i].price * _shoppingcartInfo[i].quantity;
      }
    }

    return total;
  }
}
