import 'package:faker/faker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_application_1/classData/order_item_information.dart';
import 'package:flutter_application_1/classData/shopping_cart_information.dart';

class OrderItemProvider extends ChangeNotifier {
  final List<OrderItemInformation> _items = [];

  List<OrderItemInformation> get getOrderItems {
    return [..._items];
  }

  void addOrder(List<ShoppingCartInformation> shoppingCart) {
    _items.add(OrderItemInformation(
      id: DateTime.now().toString(),
      title: faker.sport.name(),
      price: shoppingCart.fold(0, (previousValue, element) {
        return previousValue + element.price;
      }),
      product: [...shoppingCart],
      createAt: DateTime.now(),
      updateAt: DateTime.now(),
    ));
    notifyListeners();
  }
}
