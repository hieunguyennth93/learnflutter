import 'package:flutter_application_1/classData/shopping_cart_information.dart';

class OrderItemInformation {
  final String id;
  final String title;
  final double price;
  final List<ShoppingCartInformation> product;
  final DateTime createAt;
  final DateTime updateAt;

  OrderItemInformation({
    required this.id,
    required this.title,
    required this.price,
    required this.product,
    required this.createAt,
    required this.updateAt,
  });
}
