class ShoppingCartInformation {
  String id;
  String title;
  double price;
  String url;
  int quantity;
  ShoppingCartInformation(
      {required this.id,
      required this.title,
      required this.price,
      required this.url,
      required this.quantity});
}
